package ca.xsimo.webauth;

import java.security.Principal;

public class BasicGroup implements Principal {
	
	@Override
	public String getName() {
		return "user";
	}
	
}
