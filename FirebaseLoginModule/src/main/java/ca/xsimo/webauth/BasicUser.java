package ca.xsimo.webauth;

import java.security.Principal;

public class BasicUser implements Principal{
	
	private String name;
	
	public BasicUser(final String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}

}
