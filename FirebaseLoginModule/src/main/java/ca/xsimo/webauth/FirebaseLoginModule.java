package ca.xsimo.webauth;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import java.io.*;
import java.security.Principal;
import java.util.Map;
import java.util.Set;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

public class FirebaseLoginModule implements LoginModule, Serializable {
	
	private static final long serialVersionUID = 1698743935L;
	private Subject subject;
	private String username;
	private CallbackHandler callbackHandler;
	
	protected final Log log = LogFactory.getLog(this.getClass());
	
	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState,
	                       Map<String, ?> options) {
		this.subject = subject;
		this.callbackHandler = callbackHandler;
	}
	
	@Override
	public boolean login() throws LoginException {
		Callback cs[] = new javax.security.auth.callback.Callback[1];
		cs[0] = new javax.security.auth.callback.NameCallback("unused username prompt");
		try {
			callbackHandler.handle(cs);
		} catch (IOException | UnsupportedCallbackException e) {
			log.error("LoginModule error fetching login infos", e);
			throw new LoginException(e.getMessage());
		}
		username = ((javax.security.auth.callback.NameCallback)cs[0]).getName();
		log.info("FirebaseLoginModule logging in USERNAME = " + username);
		
		
		
		return true;
	}
	
	@Override
	public boolean commit() throws LoginException {
		Set<Principal> principals = subject.getPrincipals();
		for(Principal p : principals){
			log.info("principal with class "+p.getClass()+"obtained with login module subject = " + p.getName());
		}
		principals.add(new BasicGroup());
		principals.add(new BasicUser(username));
		return true;
	}
	
	@Override
	public boolean abort() throws LoginException {
		Set<Principal> principals = subject.getPrincipals();
		for(Principal p : principals){
			if(p.getClass().equals(BasicGroup.class) || p.getClass().equals(BasicUser.class)){
				principals.remove(p);
			}
		}
		return true;
	}
	
	@Override
	public boolean logout() throws LoginException {
		Set<Principal> principals = subject.getPrincipals();
		for(Principal p : principals){
			if(p.getClass().equals(BasicGroup.class) || p.getClass().equals(BasicUser.class)){
				principals.remove(p);
			}
		}
		log.info("FirebaseLoginModule logging out USERNAME = " + username);
		return true;
	}
}
