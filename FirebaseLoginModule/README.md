# Firebase Login Module

1. simply add a firebase.json file to the classpath that contains a service acco0unt json key file ( https://cloud.google.com/iam/docs/keys-create-delete?hl=fr )

1. make sure the server.xml of tomcat declares a jaasrealm

	`<Realm className="org.apache.catalina.realm.JAASRealm" appName="XsimoWebAuthLogin" userClassNames="ca.xsimo.webauth.BasicUser" roleClassNames="ca.xsimo.webauth.BasicGroup"/>`

there may be different level (engine, host, app) at which it is possible to declare that realm

1. ensure tomcat starts with the environmeet property (can be a CATALINA_OPTS)

	`-Djava.security.auth.login.config=/some/path/jaas.config`

where jaas.config contains:

	`XsimoWebAuthLogin {
     	ca.xsimo.webauth.FirebaseLoginModule required;
     };`