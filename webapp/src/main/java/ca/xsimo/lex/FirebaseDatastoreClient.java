package ca.xsimo.lex;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

public class FirebaseDatastoreClient {
	static final Log log = LogFactory.getLog(FirebaseDatastoreClient.class);
	static FirebaseApp firebaseApp;
	public static LetterFrequencyUrl record(String md5Signature, String url, String username) throws LoginException{
		InputStream serviceAccount = null;
		try {
			if (firebaseApp == null) {
				serviceAccount = Thread.currentThread().getContextClassLoader().getResourceAsStream("firebase.json");
				FirebaseOptions options = new FirebaseOptions.Builder()
						.setCredentials(GoogleCredentials.fromStream(serviceAccount))
						.build();
				firebaseApp = FirebaseApp.initializeApp(options);
			}
			
			Firestore db = FirestoreClient.getFirestore();
			try {
				DocumentSnapshot documentSnapshot = db.collection("letterFrequencies").document(md5Signature).get().get();
				LetterFrequencyUrl lfu = documentSnapshot.toObject(LetterFrequencyUrl.class);
				if (lfu == null) lfu = new LetterFrequencyUrl(url);
				Request request = new Request();
				request.setRequester(username);
				request.setDate_visited(Timestamp.now());
				lfu.requests.add(request);
				DocumentReference newLfu = db.collection("letterFrequencies").document(md5Signature);
				ApiFuture<WriteResult> result = newLfu.set(lfu);
				WriteResult writeResult = result.get();
				log.warn("Request from " + username + " for " + md5Signature + " recorded at " + writeResult.getUpdateTime());
				return lfu;
			} catch (InterruptedException | ExecutionException e) {
				log.error(e.getMessage(), e);
				throw new LoginException(e.getMessage());
			}
		} catch (IOException e) {
			log.error("configuration error", e);
			throw new LoginException(e.getMessage());
		}
	}
	
	public static Timestamp punch(String username) throws LoginException {
		
		InputStream serviceAccount = null;
		try {
			if(firebaseApp==null) {
				serviceAccount = Thread.currentThread().getContextClassLoader().getResourceAsStream("firebase.json");
				FirebaseOptions options = new FirebaseOptions.Builder()
						.setCredentials(GoogleCredentials.fromStream(serviceAccount))
						.build();
				firebaseApp = FirebaseApp.initializeApp(options);
			}
			
			Firestore db = FirestoreClient.getFirestore();
			
			try {
				DocumentSnapshot documentSnapshot = db.collection("logins").document(username).get().get();
				LoginHistory loginHistory = documentSnapshot.toObject(LoginHistory.class);
				if(loginHistory==null) loginHistory = new LoginHistory();
				Timestamp timestamp = Timestamp.now();
				Visit v = new Visit(timestamp);
				loginHistory.logins.add(v);
				DocumentReference newLoginDoc = db.collection("logins").document(username);
				ApiFuture<WriteResult> result = newLoginDoc.set(loginHistory);
				WriteResult writeResult = result.get();
				log.warn("login for " + username +" recorded at "+writeResult.getUpdateTime());
				
				return timestamp;
				
			}catch(InterruptedException | ExecutionException e){
				log.error(e.getMessage(),e);
				throw new LoginException(e.getMessage());
			}
			
		} catch (IOException e) {
			log.error("configuration error", e);
			throw new LoginException(e.getMessage());
		}
		
	}
	public static void punchOut(HttpServletRequest request) throws LoginException{
		InputStream serviceAccount = null;
		try {
			if(firebaseApp==null) {
				serviceAccount = Thread.currentThread().getContextClassLoader().getResourceAsStream("firebase.json");
				FirebaseOptions options = new FirebaseOptions.Builder()
						.setCredentials(GoogleCredentials.fromStream(serviceAccount))
						.build();
				firebaseApp = FirebaseApp.initializeApp(options);
			}
			
			Firestore db = FirestoreClient.getFirestore();
			Timestamp timestamp = (Timestamp) request.getSession().getAttribute("login_history");
			try{
				DocumentSnapshot documentSnapshot = db.collection("logins").document(request.getUserPrincipal().getName()).get().get();
				LoginHistory loginHistory = documentSnapshot.toObject(LoginHistory.class);
				if(loginHistory==null){
					log.error("Unable to locate login history for user "+request.getUserPrincipal().getName());
					return;
				}
				boolean found = false;
				for(Visit v : loginHistory.logins){
					if(v.in.equals(timestamp)){
						v.setOut(Timestamp.now());
						found = true;
						break;
					}
				}
				if(!found){
					throw new LoginException("not able to record logout for user "+request.getUserPrincipal().getName());
				}
				DocumentReference newLoginDoc = db.collection("logins").document(request.getUserPrincipal().getName());
				ApiFuture<WriteResult> result = newLoginDoc.set(loginHistory);
				WriteResult writeResult = result.get();
				log.warn("logout for " + request.getUserPrincipal().getName() +" recorded at "+writeResult.getUpdateTime());
			}catch(InterruptedException | ExecutionException e){
				log.error(e.getMessage(),e);
				throw new LoginException(e.getMessage());
			}
		} catch (IOException e) {
			log.error("configuration error", e);
			throw new LoginException(e.getMessage());
		}
	}
	
}
