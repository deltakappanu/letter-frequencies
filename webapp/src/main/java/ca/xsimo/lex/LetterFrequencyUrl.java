package ca.xsimo.lex;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.annotation.DocumentId;
import com.google.cloud.firestore.annotation.PropertyName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class LetterFrequencyUrl implements Serializable {
	
	static final long serialVersionUID = 1701301310L;
			
	@DocumentId
	String docId;
	
	@PropertyName("url")
	String url;
	
	@PropertyName("date_created")
	Timestamp date_created;
	
	@PropertyName("requests")
	List<Request> requests;
	
	public LetterFrequencyUrl(){
		this.requests = new ArrayList<>();
	}
	
	public LetterFrequencyUrl(String url){
		this.requests = new ArrayList<>();
		this.date_created = Timestamp.now();
		this.url = url;
	}
	
	public Timestamp getDate_created() {
		return date_created;
	}
	
	public void setDate_created(Timestamp date_created) {
		this.date_created = date_created;
	}
	
	public List<Request> getRequests() {
		return requests;
	}
	
	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}
	
	public String getDocId() {
		return docId;
	}
	
	public void setDocId(String docId) {
		this.docId = docId;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}
