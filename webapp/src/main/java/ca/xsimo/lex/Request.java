package ca.xsimo.lex;

import com.google.cloud.Timestamp;
import com.google.cloud.firestore.annotation.PropertyName;
import java.io.Serializable;

public class Request implements Serializable {
	static final long serialVersionUID = 1701301320L;
	@PropertyName("date_visited")
	Timestamp date_visited;
	
	@PropertyName("requester")
	String requester;
	
	public String getRequester() {
		return requester;
	}
	
	public void setRequester(String requester) {
		this.requester = requester;
	}
	
	public Timestamp getDate_visited() {
		return date_visited;
	}
	
	public void setDate_visited(Timestamp date_visited) {
		this.date_visited = date_visited;
	}
}
