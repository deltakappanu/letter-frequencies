package ca.xsimo.lex;

import com.google.cloud.Timestamp;
import com.google.cloud.firestore.annotation.DocumentId;
import com.google.cloud.firestore.annotation.PropertyName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoginHistory implements Serializable {
	
	static final long serialVersionUID = 1701301263L;
	
	@DocumentId
	String docId;
	
	@PropertyName("logins")
	List<Visit> logins;
	
	public LoginHistory(){
		logins = new ArrayList<>();
	}
	
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getDocId(){
		return docId;
	}
	
	
	public List<Visit> getLogins() {
		return logins;
	}
	
	public void setLogins(List<Visit> logins) {
		this.logins = logins;
	}
}
