package ca.xsimo.lex;

import com.google.cloud.Timestamp;
import com.google.cloud.firestore.annotation.PropertyName;

import java.io.Serializable;

public class Visit implements Serializable {
	static final long serialVersionUID = 1701304767L;
	@PropertyName("in")
	Timestamp in;
	@PropertyName("out")
	Timestamp out;
	public Visit(){
	
	}
	public Visit(Timestamp in){
		this.in = in;
	}
	public Timestamp getIn() {
		return in;
	}
	
	public void setIn(Timestamp in) {
		this.in = in;
	}
	
	public Timestamp getOut() {
		return out;
	}
	
	public void setOut(Timestamp out) {
		this.out = out;
	}
}
