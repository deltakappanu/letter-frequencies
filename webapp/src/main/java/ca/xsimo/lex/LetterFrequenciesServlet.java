package ca.xsimo.lex;

import com.google.cloud.Timestamp;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.security.auth.login.LoginException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map.Entry;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class LetterFrequenciesServlet extends HttpServlet{
	final static String PREFIX = "ca.xsimo.lex.lf.";
	final static int MD5_LENGTH = 32;
	final static String SUFFIX = ".cache";
	final static int BARCHART_MAX_WIDTH = 500;
	final static String validHexChars = "0123456789abcdef";
	final static int MAX_STRING = 10;
	private static String CLIENT_ID;
	
	static final Log logger = LogFactory.getLog(LetterFrequenciesServlet.class);
	
	public static void main(String [] args) throws IOException, ClassNotFoundException, NoSuchAlgorithmException {
		String defaultSite = "https://laws.justice.gc.ca/fra/lois/A-1/TexteComplet.html";
		System.out.println(parse(getMD5(defaultSite), defaultSite));
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("recent", populateRecentUrls());
		String url = request.getParameter("url");
		String recent_url = request.getParameter("recent_url");
		if((url==null || url.equals("")) && (recent_url==null || recent_url.equals(""))){
			request.setAttribute("errorMsg", "url is empty");
			throw new ServletException();
		}
		if(recent_url!=null && !recent_url.equals("")){
			if(!validHex(recent_url)){
				request.setAttribute("errorMsg", "recent_url has to be within the offered selection");
				throw new ServletException();
			}

			try {
				UrlAndText uat = getUrlAndText(recent_url);
				url = uat.url;
			}catch(ClassNotFoundException cnfe){

			}
		}
		if (!url.startsWith("https://")) {
			request.setAttribute("errorMsg", "url must begin with https://");
			throw new ServletException();
		}
		try{
			new URL(url);
		} catch (MalformedURLException mue){
			request.setAttribute("errorMsg", "malformed url");
			throw new ServletException();
		}
		String answer = null;
		try {
			String md5 = getMD5(url);
			FirebaseDatastoreClient.record(md5, url, request.getUserPrincipal().getName());
			answer = parse(md5, url);
		}catch(ClassNotFoundException|NoSuchAlgorithmException nse){
			// should not happen
			throw new ServletException("");
		} catch (LoginException e) {
			log(e.getMessage(), e);
			throw new ServletException("LoginException while attempting to record the request into Google Cloud Firestore");
		}
		request.setAttribute("answer", answer);

		ServletContext ctxt = getServletContext();
		RequestDispatcher rd = ctxt.getRequestDispatcher("/index.jsp");
		rd.forward(request, response);
	}

	private static boolean validHex(String r){
		for(int i = 0; i< r.length(); i++){
			char sanitized_chars = r.charAt(i);
			if(!validHexChars.contains(""+sanitized_chars)) {
				return false;
			}
		}
		return true;
	}

	private static String getMD5(String yourString) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] bytesOfMessage = yourString.getBytes("UTF-8");

		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] theMD5digest = md.digest(bytesOfMessage);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < theMD5digest.length; ++i) {
			sb.append(byteToHex(theMD5digest[i]));
		}
		return sb.toString();
	}

	private static String byteToHex(byte num) {
		char[] hexDigits = new char[2];
		hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
		hexDigits[1] = Character.forDigit((num & 0xF), 16);
		return new String(hexDigits);
	}

	private static File getFile(String md5){
		return new File(System.getProperty("java.io.tmpdir")+File.separator+PREFIX+md5+SUFFIX);
	}

	private static UrlAndText getUrlAndText(String md5) throws IOException, ClassNotFoundException {
		File cachedFile = getFile(md5);
		if(!cachedFile.exists()) throw new IOException("file not found"); // should not happen
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cachedFile));
		UrlAndText uao = (UrlAndText) ois.readObject();
		ois.close();
		return uao;
	}

	private static String parse(String md5, String url) throws IOException, NoSuchAlgorithmException, ClassNotFoundException {
		Document doc = null;
		UrlAndText uao;
		try {
			uao = getUrlAndText(md5);
			System.out.println("using cached version of " + uao.url);
			doc = Jsoup.parse(uao.texte);
		}catch(Exception e){
			doc = Jsoup.parse(new URL(url), 5000);
			File cachedFile = getFile(md5);
			System.out.println("caching "+url+" on filesystem:///" + cachedFile.getAbsolutePath());
			cachedFile.createNewFile();
			String title = "Untitled";
			if(doc.head().getElementsByTag("title").size()>0) title = doc.head().getElementsByTag("title").get(0).text();
			uao = new UrlAndText(url, doc.outerHtml(), title);
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(cachedFile));
			oos.writeObject(uao);
			oos.flush(); oos.close();
		}

		String texte = doc.text();

		int[] frequences = new int[26];
		for(int i = 0; i<26; i++) frequences[i] = 0;
		for(int i = 0; i< texte.length(); i++){
			char c = texte.charAt(i);
			if((c<='Z' && c>='A') || (c<='z' && c>= 'a')){
				if(c<='z' && c>= 'a') c-= 'a';
				else c -= 'A';
				frequences[c]++;
			}
		}
		java.util.HashMap<Character, Integer> valz = new HashMap<>();
		int total = 0;
		for(int i = 0; i<26; i++){
			char d = (char)('A'+i);
			valz.put(new Character(d), frequences[i]);
			total+=frequences[i];
		}
		ArrayList<Entry<Character, Integer>> list = new ArrayList<>();
		for(Entry<Character, Integer> e : valz.entrySet()) list.add(e);
		Collections.sort(list, (a, b) -> b.getValue().compareTo(a.getValue()));
		StringBuffer sb = new StringBuffer();
		int barlen = (int)Math.ceil(texte.length() / (0.0+ BARCHART_MAX_WIDTH));
		sb.append("<a target=\"_blank\" href="+url+"\">"+url+"<br/>"+uao.title+"</a><br/>");
		sb.append("<br>Total of <b>"+texte.length() +"</b> character <br/>(one \"|\" equals to a maximum of "+barlen + " occurrences)<br>");
		sb.append("<br><br><pre>");
		for(Entry<Character, Integer> e : list){
			sb.append(e.getKey() + " ");
			int bars = 0;
			if(e.getValue()>0) {
				double portion = e.getValue() / (total + 0.0);
				portion *= BARCHART_MAX_WIDTH;
				bars = (int) Math.ceil(portion);
			}
			System.out.println(e.getKey() + " " + bars +" = "+e.getValue()+"/"+total);
			for(int b= 0; b<bars; b++) sb.append("|");
			sb.append( " " + e.getKey()+"\r\n");
		}
		sb.append("</pre>");
		return sb.toString();
	}

	public static HashMap<String, String> populateRecentUrls(){
		HashMap<String, String> map = new HashMap<>();
		HashMap<String, UrlAndText> mapInternal = new HashMap<>();
		File [] filesList = new File(System.getProperty("java.io.tmpdir")).listFiles();
		int currentCount = 0;
		for(File f : filesList){
			if(f.getName().startsWith(PREFIX) && f.getName().endsWith(SUFFIX) && f.getName().length()==(PREFIX.length() + MD5_LENGTH + SUFFIX.length() )){
				currentCount++;
				String hex = f.getName().substring(PREFIX.length(),PREFIX.length() + MD5_LENGTH);
				if(!validHex(hex)) continue;
				try {
					UrlAndText uat = getUrlAndText(hex);
					if(currentCount>MAX_STRING){
						f.delete();
					}else {

						mapInternal.put(hex, uat);
					}
				}catch(IOException | ClassNotFoundException e){
					continue;
				}
			}
		}

		ArrayList<Entry<String, UrlAndText>> entryList = new ArrayList<>();
		for(Entry<String, UrlAndText> e : mapInternal.entrySet()) entryList.add(e);
		Collections.sort(entryList, (a, b) -> b.getValue().timestamp.compareTo(a.getValue().timestamp));
		for(int i = MAX_STRING; i < entryList.size(); i++){
			Entry<String, UrlAndText> entry = entryList.get(i);
			mapInternal.remove(entry.getKey());
			getFile(entry.getKey()).delete();
		}
		for(String hex : mapInternal.keySet()){
			map.put(hex, mapInternal.get(hex).url);
		}
		return map;
	}

	public static String getClientId() throws IOException {
		if(CLIENT_ID!=null) return CLIENT_ID;
		String json = "";
		URL url = Thread.currentThread().getContextClassLoader().getResource("client_secret.json");
		String filepath = URLDecoder.decode(url.getPath(),"UTF-8");
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filepath),"UTF-8"));
		String line = null;
		while( (line = reader.readLine()) != null){
			json += line;
		}
		JSONObject jsonObject = new JSONObject(json);
		CLIENT_ID = jsonObject.getJSONObject("web").getString("client_id");
		return CLIENT_ID;
	}
	
	public static void loginFromJsp(HttpServletRequest request){
		Object firestoreLoginTimestamp = request.getSession().getAttribute("login_history");
		if(firestoreLoginTimestamp==null || !(firestoreLoginTimestamp instanceof Timestamp)){
			try {
				Timestamp timestamp = FirebaseDatastoreClient.punch(request.getUserPrincipal().getName());
				request.getSession().setAttribute("login_history", timestamp);
			}catch(LoginException le){
				logger.error(le.getMessage(), le);
			}
		}
	}
	
}
