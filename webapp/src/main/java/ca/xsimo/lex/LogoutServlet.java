package ca.xsimo.lex;

import com.google.cloud.Timestamp;

import javax.security.auth.login.LoginException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class LogoutServlet extends HttpServlet {
	
	Log logger = LogFactory.getLog(LogoutServlet.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			FirebaseDatastoreClient.punchOut(request);
		}catch(LoginException le){
			logger.error(le.getMessage(), le);
		}
		HttpSession session = request.getSession();
		session.invalidate();
		request.logout();
		String url="/logout.jsp";
		ServletContext ctxt = getServletContext();
		RequestDispatcher rd = ctxt.getRequestDispatcher(url);
		rd.forward(request, response);
	}
}
