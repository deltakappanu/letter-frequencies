package ca.xsimo.lex;

import java.io.Serializable;

public class UrlAndText implements Serializable {
	static final long serialVersionUID = 1698467404L;
	String texte;
	String url;
	String title;
	Long timestamp;
	public UrlAndText(String url, String texte, String title){
		this.texte = texte;
		this.url = url;
		this.title = title;
		this.timestamp = System.currentTimeMillis();
	}
    public UrlAndText(String url, String texte, String title, long timestamp){
		this.texte = texte;
		this.url = url;
	    this.title = title;
		this.timestamp = timestamp;
	}

}