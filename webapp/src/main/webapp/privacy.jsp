<!DOCTYPE>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="icon" href="favicon.ico" type="image/x-icon" sizes="16x16">

	<!-- LettersFrequencies custom styles and scripts-->
	<link rel="stylesheet" href="style.css"/>
</head>
<body>
	<div >
	<div id="privacy">
		<h3>Conditions d'utilisation</h3>
		<b>A. R&Ocirc;LES</b><br>
		<ul>
			<li>Propriétaires de l'application, Nous, Nos: Creations Xsimo Enr.</li>
			<li>Utilisateur: Toute personne s'authentifiant avec Google et accordant des autorisations à cette application</li>
		</ul>
		<b>B. LES CONDITIONS</b><br>
		<p>L'application letters permet de soumettre des urls via le formulaire en ligne et d'observer des statistiques à propos des ces urls.
		L'authentification est nécessaire car l'accès est accordé sur demande et sur invitation seulement pour éviter les abus.<br>
		Il s'agit d'un service gratuit sauf accord prévalent entre un ou des utilisateur et les propriétaires.<br>
		Les autorisations demandé par l'application se résume à lire l'adresse courriel et le nom de l'utilisateur<br>
		Ces informations ne seront pas partagés, loués ou vendus et sont seulement requise pour l'identification de l'utilisateur et pour lui donner accès à l'application.
		<br/>
		<h3>Terms of Services</h3>
		<b>A. ROLES:</b><br/>
		<ul>
			<li>Owners of the application, us, our: Creations Xsimo Enr.</li>
			<li>User: Person authenticating with Google and granting this app some authorizations</li>
		</ul>
		<p>This app allows to submit some urls through the online form and see some statistics about those urls.
		The authentication is required because access is granted on demand and on invitation only to prevent abuse.<br>
		It is a free service unless otherwise stated by a superseeding contract between user(s) and the owners.</br>
		The authorization asked by the app are onlyu to read the email address et the user name</br>
		Those information will not be shared, rent or sold and are only needed to identify the user and grant him access to this app.
	</div>
	</body>
</html>
