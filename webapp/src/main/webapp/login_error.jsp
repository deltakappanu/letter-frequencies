
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="WEB-INF/jsp/head.jsp"/>
<center>
	<i>Wrong username/password combination</i>
	<br>
	<br>
	<a target="_self" href="<%= request.getContextPath() %>/login.jsp">Back</a>
</center>
<jsp:include page="WEB-INF/jsp/footer.jsp"/>
