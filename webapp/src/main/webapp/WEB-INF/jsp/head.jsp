<%@ page import="java.util.Enumeration" %>
<html>
<head>
	<title>Letter Frequencies</title>

	<script src="https://accounts.google.com/gsi/client" async></script>

</head>
<body>
<div class="content">
	<div id="login_block" style="float:right;">
		<%if(request.getUserPrincipal()!=null){%>logged in as
			<%= request.getSession().getAttribute("displayName")%>
			<br/>
			<%=request.getUserPrincipal().getName()%>
			<br/>
			<a href="#"><div id="signout_button" class="g_id_signout" style="text-align:right;cursor:pointer;">Sign Out</div></a>
		<%}%>
		<script>
			const button = document.getElementById('signout_button');
			button.onclick = () => {
				google.accounts.id.disableAutoSelect();
				location.href = "<%=request.getContextPath()%>/logout";
			}
		</script>

	</div>
	<div style="clear:both;"></div>
	<h1>Letter Frequencies</h1>
