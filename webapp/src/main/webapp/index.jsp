<%@ page import="java.util.HashMap" %>
<%@ page import="ca.xsimo.lex.LetterFrequenciesServlet" %>
<jsp:include page="WEB-INF/jsp/head.jsp"/>
<%LetterFrequenciesServlet.loginFromJsp(request);%>
<form action="<%=request.getContextPath()%>/letters">
	<select name="recent_url">
		<option value="">select recent</option>
		<% HashMap<String,String> map = new HashMap<String, String>();
			if(request.getAttribute("recent")!=null && !request.getAttribute("recent").equals("")) {
				map = (HashMap<String, String>) (request.getAttribute("recent"));
			}else{
				map = LetterFrequenciesServlet.populateRecentUrls();
			}
			for(String key : map.keySet()){%>
			<option value="<%=key%>"><%=map.get(key)%></option>
		<%}%>
	</select>
	<br/>
	or enter your own
	<br/>
	<label for="url">URL</label>
	<input type="text" id="url" name="url"/>
	<br>
	<input type="submit"/>
</form>
<br/>
<br/>
<%if(request.getAttribute("answer")!=null && !request.getAttribute("answer").equals("")){%>
	<%=request.getAttribute("answer")%>
<%}%>
<jsp:include page="WEB-INF/jsp/footer.jsp"/>