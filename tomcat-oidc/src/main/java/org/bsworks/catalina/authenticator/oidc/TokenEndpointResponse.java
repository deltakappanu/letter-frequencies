package org.bsworks.catalina.authenticator.oidc;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;

/**
 * OP token endpoint response.
 */
public final class TokenEndpointResponse {
	
	/**
	 * Response HTTP status code.
	 */
	public final int responseCode;
	
	/**
	 * Response date.
	 */
	public final Date responseDate;
	
	/**
	 * Response body.
	 */
	public final JSONObject responseBody;
	
	
	/**
	 * Create new object representing a response.
	 *
	 * @param responseCode Response HTTP status code.
	 * @param responseDate Response date.
	 * @param responseBody Response body.
	 */
	public TokenEndpointResponse(final int responseCode, final long responseDate,
	                             final JSONObject responseBody) {
		
		this.responseCode = responseCode;
		this.responseDate = new Date(
				responseDate != 0 ? responseDate : System.currentTimeMillis());
		this.responseBody = responseBody;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		return "status: " + this.responseCode + ", date: "
				+ DateFormat.getDateTimeInstance().format(this.responseDate)
				+ ", body: " + this.responseBody;
	}
}
