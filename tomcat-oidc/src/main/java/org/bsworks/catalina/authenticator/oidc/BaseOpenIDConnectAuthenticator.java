package org.bsworks.catalina.authenticator.oidc;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.v2.ApacheHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.Session;
import org.apache.catalina.authenticator.Constants;
import org.apache.catalina.authenticator.FormAuthenticator;
import org.apache.catalina.authenticator.SavedRequest;
import org.apache.catalina.connector.Request;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.buf.HexUtils;
import org.apache.tomcat.util.descriptor.web.LoginConfig;
import org.bsworks.util.json.JsonUtilities;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


/**
 * Base <em>OpenID Connect</em> authenticator implementation for different versions of <em>Tomcat</em>.
 *
 * @author Lev Himmelfarb
 */
public abstract class BaseOpenIDConnectAuthenticator extends FormAuthenticator {
	
	// Name of request attribute made available to the login page that maps configured OP issuer IDs to the corresponding authorization endpoint URLs.
	public static final String AUTHEPS_ATT = "org.bsworks.oidc.authEndpoints";

	// Name of request attribute made available to the login page that tells if the form-based authentication is disabled.
	public static final String NOFORM_ATT = "org.bsworks.oidc.noForm";

	//Name of request attribute made available on the login error page that contains the error descriptor.
	public static final String AUTHERROR_ATT = "org.bsworks.oidc.error";

	// Name of session attribute used to store the {@link Authorization} object.
	public static final String AUTHORIZATION_ATT = "org.bsworks.oidc.authorization";

	// UTF-8 charset.
	private static final Charset UTF8 = Charset.forName("UTF-8");

	// URL-safe base64 decoder.
	private static final Base64.Decoder BASE64URL_DECODER = Base64.getUrlDecoder();

	// Base64 encoder.
	private static final Base64.Encoder BASE64_ENCODER = Base64.getEncoder();

	// Name of the HTTP session note used to store the {@link Authorization} object.
	private static final String SESS_OIDC_AUTH_NOTE = "org.bsworks.catalina.session.AUTHORIZATION";

	// Name of the HTTP session note used to store the state value.
	private static final String SESS_STATE_NOTE = "org.bsworks.catalina.session.STATE";

	// Pattern for the state parameter.
	private static final Pattern STATE_PATTERN = Pattern.compile("^(\\d+)Z(.+)");

	// Pattern used to parse providers configuration and convert it into JSON.
	private static final Pattern OP_CONF_LINE_PATTERN = Pattern.compile("(\\w+)\\s*:\\s*(?:'([^']*)'|([^\\s,{}]+))");

	protected final Log log = LogFactory.getLog(this.getClass());

	// Virtual host base URI.
	protected String hostBaseURI;
	
	// Providers configuration.
	protected String providers;

	// Name of the claim in the ID Token used as the username in the users realm. Can be overridden for specific OPs.
	protected String usernameClaim = "sub";

	// Space separated list of scopes to add to "openid" scope in the authorization endpoint request. Can be overridden for specific OPs.
	protected String additionalScopes;

	// Tells if the form-based authentication is disabled.
	protected boolean noForm = false;

	// HTTP connect timeout for OP endpoints and configuration document URL. Can be overridden for specific OPs.
	protected int httpConnectTimeout = 5000;

	// HTTP read timeout for OP endpoints and configuration document URL. Can be overridden for specific OPs.
	protected int httpReadTimeout = 5000;
	
	// Secure random number generator.
	private final SecureRandom rand = new SecureRandom();

	// Configured OpenID Connect Provider descriptors.
	private List<OPDescriptor> opDescs;

	// OpenID Connect Provider configurations provider.
	private OPConfigurationsProvider ops;
	
	/**
	 * Verify that the authenticator is running under a compatible version of
	 * Tomcat.
	 *
	 * @throws LifecycleException If the Tomcat version is incompatible.
	 */
	protected abstract void ensureTomcatVersion() throws LifecycleException;

	@Override
	protected synchronized void startInternal()  throws LifecycleException {
		this.log.info("tomcat-oidcauth :: startInternal");

		// verify Tomcat version
		this.ensureTomcatVersion();

		// verify that providers are configured
		if (this.providers == null) {
			this.log.info("tomcat-oidcauth :: startInternal, provider is null");
			throw new LifecycleException("OpenIDConnectAuthenticator requires \"providers\" property.");
		}

		// parse provider definitions and create the configurations provider
		final String providersConf = this.providers.trim();
		if (providersConf.startsWith("[")) {
			final StringBuffer providersConfJSONBuf = new StringBuffer(512);
			final Matcher m = OP_CONF_LINE_PATTERN.matcher(providersConf);
			while (m.find()) {
				m.appendReplacement(providersConfJSONBuf, Matcher.quoteReplacement("\"" + m.group(1) + "\": \"" + (m.group(2) != null ? m.group(2) : m.group(3)) + "\""));
			}
			m.appendTail(providersConfJSONBuf);
			final String providersConfJSON = providersConfJSONBuf.toString();
			try {
				this.log.info("tomcat-oidcauth :: parsing configuration JSON: " + providersConfJSON);
				final JSONArray opDefs = new JSONArray(new JSONTokener(new StringReader(providersConfJSON)));
				final int numOPs = opDefs.length();
				this.opDescs = new ArrayList<>(numOPs);
				for (int i = 0; i < numOPs; i++) {
					final Object opDef = opDefs.opt(i);
					if ((opDef == null) || !(opDef instanceof JSONObject)) {
						this.log.info("tomcat-oidcauth :: expected object at provider array # "+i);
						throw new LifecycleException("Expected an object at OpenIDConnectAuthenticator \"providers\" array element " + i + ".");
					}
					this.opDescs.add(new OPDescriptor((JSONObject) opDef, this.usernameClaim, this.additionalScopes, this.httpConnectTimeout, this.httpReadTimeout));
				}
			} catch (final JSONException e) {
				this.log.info("tomcat-oidcauth :: startInternal cound not parse provider");
				throw new LifecycleException("OpenIDConnectAuthenticator could not parse \"providers\" property.", e);
			}
		} else { // deprecated syntax
			final String[] defs = providersConf.split("\\s+");
			this.opDescs = new ArrayList<>(defs.length);
			for (final String def : defs)
				this.opDescs.add(new OPDescriptor(def, this.usernameClaim, this.additionalScopes, this.httpConnectTimeout, this.httpReadTimeout));
		}
		this.ops = new OPConfigurationsProvider(this.opDescs);

		// preload provider configurations and detect any errors
		try {
			for (final OPDescriptor opDesc : this.opDescs)
				if (!opDesc.isOptional())
					this.ops.getOPConfiguration(opDesc.getIssuer());
		} catch (final IOException | JSONException e) {
			this.log.info("tomcat-oidcauth :: startInternal, oidc could not load oidc provider configuration");
			throw new LifecycleException("OpenIDConnectAuthenticator could not load OpenID Connect Provider configuration.", e);
		}

		// proceed with initialization
		super.startInternal();
	}
	
	/**
	 * Perform authentication.
	 *
	 * @param request The request.
	 * @param response The response.
	 *
	 * @return Authentication result.
	 *
	 * @throws IOException If an I/O error happens.
	 */
	@Override
	protected boolean doAuthenticate(final Request request, final HttpServletResponse response) throws IOException {
		// try to reauthenticate if caching principal is disabled
		if (!this.cache && this.reauthenticateNoCache(request, response))
			return true;
		
		// check if resubmit after successful authentication
		if (this.matchRequest(request))
			return this.processResubmit(request, response);
		
		// check if already authenticated
		if (this.checkForCachedAuthentication(request, response, true))
			return true;
		
		// the request is not authenticated:
		
		// determine if authentication submission
		final String requestURI = request.getDecodedRequestURI();
		final boolean loginAction = ( requestURI.startsWith(request.getContextPath()) && requestURI.endsWith(Constants.FORM_ACTION));
		
		// check if regular unauthenticated request, not a submission
		if (!loginAction) {
			this.processUnauthenticated(request, response);
			return false;
		}
		
		// authentication submission (either form or OP response redirect):
		
		// acknowledge the request
		request.getResponse().sendAcknowledgement();
		
		// set response character encoding
		if (this.characterEncoding != null) request.setCharacterEncoding(this.characterEncoding);
		
		// get current session and check if expired
		final Session session = request.getSessionInternal(false);
		if (session == null) {
			
			// log using container log (why container?)
			this.log.debug("user took so long to log on the session expired");
			
			// process expired session
			this.processExpiredSession(request, response);
			
			// done, authentication failure
			return false;
		}
		this.log.debug("existing session id " + session.getId());
		
		// the authenticated user
		AuthedUser authedUser = null;
		
		// check if OIDC authentication response or form submission
		if ((request.getParameter("code") != null) || (request.getParameter("error") != null) ||
				(request.getParameter("credential")!=null && !request.getParameter("credential").equals(""))) {
			
			this.log.info("tomcat-oidcauth :: performAuthentication, error or code parameter detected");
			authedUser = this.processAuthResponse(session, request);
			
		} else if (!this.noForm) { // form submission
			
			this.log.info("tomcat-oidcauth :: performAuthentication, credential or code not present in request and not noForm");
			Enumeration<String> parameterNames = request.getParameterNames();
			String parameterName; int i = 1;
			while(parameterNames.hasMoreElements()){
				parameterName = parameterNames.nextElement();
				this.log.error("tomcat-oidc-auth :: parameter name "+i+" name="+parameterName+", value="+request.getParameter(parameterName));
				i++;
			}
			authedUser = this.processAuthFormSubmission(session,
					request.getParameter(Constants.FORM_USERNAME),
					request.getParameter(Constants.FORM_PASSWORD));
		}
		
		// check if authentication failure
		if (authedUser == null) {
			this.log.info("tomcat-oidcauth :: authed user is null");
			this.forwardToErrorPage(request, response, this.context.getLoginConfig());
			return false;
		}
		
		// successful authentication
		this.log.debug("authentication of \"" + authedUser.principal.getName() + "\" was successful");
		
		// change session id (to prevent a session fixation attack)
		if (this.getChangeSessionIdOnAuthentication()) {
			final String expectedSessionId = (String) session.getNote( Constants.SESSION_ID_NOTE);
			if ((expectedSessionId == null) || !expectedSessionId.equals( request.getRequestedSessionId())) {
				this.log.debug("unable to change session id, expiring the session: expected session id is " + expectedSessionId + ", requested session id is " + request.getRequestedSessionId());
				session.expire();
				this.processExpiredSession(request, response);
				return false;
			}
		}
		
		// save the authenticated principal in our session
		this.register(request, response, authedUser.principal, HttpServletRequest.FORM_AUTH, authedUser.username, authedUser.password);
		
		// get the original unauthenticated request URI
		final String origRequestURI = this.savedRequestURL(session);
		this.log.debug("redirecting to original URI: " + origRequestURI);
		
		// if (somehow!) original URI is unavailable, go to the landing page
		if (origRequestURI == null) {
			if (!this.redirectToLandingPage(request, response))
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, sm.getString("authenticator.formlogin"));
			return false;
		}
		
		// redirect to the original URI
		request.getResponse().sendRedirect( response.encodeRedirectURL(origRequestURI), ("HTTP/1.1".equals(request.getProtocol()) ? HttpServletResponse.SC_SEE_OTHER : HttpServletResponse.SC_FOUND));
		
		// done, will be authenticated after the redirect
		return false;
	}
	
	@Override
	protected void forwardToLoginPage(final Request request, final HttpServletResponse response, final LoginConfig config) throws IOException {
		this.log.info("tomcat-oidcauth :: forwardToLoginPage");
		
		// add login configuration request attributes for the page
		this.addLoginConfiguration(request);
		
		// proceed to the login page
		super.forwardToLoginPage(request, response, config);
	}
	
	@Override
	protected void forwardToErrorPage(final Request request, final HttpServletResponse response, final LoginConfig config) throws IOException {
		this.log.info("tomcat-oidcauth :: forwardToErrorPage");
		
		// add login configuration request attributes for the page
		this.addLoginConfiguration(request);
		
		// proceed to the login error page
		super.forwardToErrorPage(request, response, config);
	}
	
	@Override
	public void logout(final Request request) {
		this.log.info("tomcat-oidcauth :: logout");
		
		final Session session = request.getSessionInternal(false);
		if (session != null) {
			session.removeNote(SESS_STATE_NOTE);
			session.removeNote(Constants.SESS_USERNAME_NOTE);
			session.removeNote(SESS_OIDC_AUTH_NOTE);
			session.removeNote(Constants.FORM_REQUEST_NOTE);
			session.removeNote("userId");
			session.removeNote("email");
			session.removeNote("emailVerified");
			session.removeNote("pictureUrl");
			session.removeNote("locale");
			session.removeNote("familyName");
			session.removeNote("givenName");
			session.getSession().removeAttribute(AUTHORIZATION_ATT);
		}
		
		super.logout(request);
	}
	
	/**
	 * If caching principal on the session by the authenticator is disabled,
	 * check if the session has authentication information (username, password
	 * or OP issuer ID) and if so, reauthenticate the user.
	 *
	 * @param request The request.
	 * @param response The response.
	 *
	 * @return {@code true} if was successfully reauthenticated and no further
	 * authentication action is required. If authentication logic should
	 * proceed, returns {@code false}.
	 */
	protected boolean reauthenticateNoCache(final Request request, final HttpServletResponse response) {
		// get session
		final Session session = request.getSessionInternal(true);

		this.log.debug("checking for reauthenticate in session " + session.getIdInternal());
		
		// check if authentication info is in the session
		final String username = (String) session.getNote(Constants.SESS_USERNAME_NOTE);
		if (username == null) return false;

		// get the rest of the authentication info
		final Authorization authorization = (Authorization) session.getNote(SESS_OIDC_AUTH_NOTE);
		final String password = (String) session.getNote(Constants.SESS_PASSWORD_NOTE);

		// get the principal from the realm (try to reauthenticate)
		Principal principal = null;
		if (authorization != null) { // was authenticated using OpenID Connect
			
			this.log.debug("reauthenticating username \"" + username + "\" authenticated by " + authorization.getIssuer());
			
			principal = this.context.getRealm().authenticate(username);
			
		} else if (password != null) { // was form-based authentication
		
			this.log.debug("reauthenticating username \"" + username + "\" using password");
			
			principal = this.context.getRealm().authenticate( username, password);
		}

		// check if could not reauthenticate
		if (principal == null) {
			this.log.debug("reauthentication failed, proceed normally");
			return false;
		}

		// successfully reauthenticated, register the principal
		this.log.debug("successfully reauthenticated username \"" + username + "\"");
		this.register(request, response, principal,
				HttpServletRequest.FORM_AUTH, username, password);

		// check if resubmit after successful authentication
		if (this.matchRequest(request)) {
			this.log.debug("reauthenticated username \"" + username + "\" for resubmit after successful authentication");
			return false;
		}

		// no further authentication action required
		return true;
	}
	
	/**
	 * Process original request resubmit after successful authentication.
	 *
	 * @param request The request.
	 * @param response The response.
	 *
	 * @return {@code true} if success, {@code false} if failure, in which case
	 * an HTTP 400 response is sent back by this method.
	 *
	 * @throws IOException If an I/O error happens communicating with the
	 * client.
	 */
	protected boolean processResubmit(final Request request, final HttpServletResponse response)  throws IOException {
		// get session
		final Session session = request.getSessionInternal(true);

		this.log.debug("restore request from session " + session.getIdInternal());

		// if principal is cached, remove authentication info from the session
		if (this.cache) {
			session.removeNote(Constants.SESS_USERNAME_NOTE);
			session.removeNote(Constants.SESS_PASSWORD_NOTE);
			session.removeNote(SESS_OIDC_AUTH_NOTE);
		}

		// try to restore original request
		if (!this.restoreRequest(request, session)) {
			this.log.debug("restore of original request failed");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return false;
		}

		// all good, no further authentication action is required
		this.log.debug("proceed to restored request");
		return true;
	}

	/**
	 * Process regular unauthenticated request. Normally, saves the request in
	 * the session and forwards to the configured login page.
	 *
	 * @param request The request.
	 * @param response The response.
	 *
	 * @throws IOException If an I/O error happens communicating with the
	 * client.
	 */
	protected void processUnauthenticated(final Request request, final HttpServletResponse response) throws IOException {
		// If this request was to the root of the context without a trailing
		// "/", need to redirect to add it else the submit of the login form
		// may not go to the correct web application
		if ((request.getServletPath().length() == 0) && (request.getPathInfo() == null)) {
			final StringBuilder location = new StringBuilder( request.getDecodedRequestURI());
			location.append('/');
			if (request.getQueryString() != null) location.append('?').append(request.getQueryString());
			response.sendRedirect(response.encodeRedirectURL(location.toString()));
			return;
		}

		// get session
		final Session session = request.getSessionInternal(true);

		this.log.debug("save request in session " + session.getIdInternal());

		// save original request in the session before forwarding to the login
		try {
			this.saveRequest(request, session);
		} catch (final IOException e) {
			this.log.debug("could not save request during authentication", e);
			response.sendError(HttpServletResponse.SC_FORBIDDEN, sm.getString("authenticator.requestBodyTooBig"));
			return;
		}

		// forward to the login page
		this.forwardToLoginPage(request, response, this.context.getLoginConfig());
	}

	/**
	 * Add request attributes for the login or the login error page.
	 *
	 * @param request The request.
	 *
	 * @throws IOException If an I/O error happens.
	 */
	protected void addLoginConfiguration(final Request request) throws IOException {
		// generate state value and save it in the session
		final byte[] stateBytes = new byte[16];
		this.rand.nextBytes(stateBytes);
		final String state = HexUtils.toHexString(stateBytes);
		request.getSessionInternal(true).setNote(SESS_STATE_NOTE, state);

		// add OP authorization endpoints to the request for the login page
		final List<AuthEndpointDesc> authEndpoints = new ArrayList<>();
		final StringBuilder buf = new StringBuilder(128);
		for (int i = 0; i < this.opDescs.size(); i++) {
			final OPDescriptor opDesc = this.opDescs.get(i);

			// get the OP configuration
			final String issuer = opDesc.getIssuer();
			final OPConfiguration opConfig = this.ops.getOPConfiguration(issuer);
			if (opConfig == null) continue;

			// construct the authorization endpoint URL
			buf.setLength(0);
			buf.append(opConfig.getAuthorizationEndpoint());
			buf.append("?scope=openid");
			final String extraScopes = opDesc.getAdditionalScopes();
			if (extraScopes != null) buf.append(URLEncoder.encode(" " + extraScopes, UTF8.name()));
			buf.append("&response_type=code");
			buf.append("&client_id=").append(URLEncoder.encode(opDesc.getClientId(), UTF8.name()));
			buf.append("&redirect_uri=").append(URLEncoder.encode(this.getBaseURL(request) + Constants.FORM_ACTION, UTF8.name()));
			buf.append("&state=").append(i).append('Z').append(state);
			final String addlParams = opDesc.getExtraAuthEndpointParams();
			if (addlParams != null) buf.append('&').append(addlParams);

			// add the URL to the map
			authEndpoints.add(new AuthEndpointDesc(opDesc.getName(), issuer, buf.toString()));
		}
		request.setAttribute(AUTHEPS_ATT, authEndpoints);

		// add no form flag to the request
		request.setAttribute(NOFORM_ATT, Boolean.valueOf(this.noForm));
	}

	/**
	 * Process login form submission.
	 *
	 * @param session The session.
	 * @param username Submitted username.
	 * @param password Submitted password.
	 *
	 * @return The authenticated user, or {@code null} if login failure.
	 */
	protected AuthedUser processAuthFormSubmission(@SuppressWarnings("unused") final Session session, final String username, final String password) {
		this.log.debug("authenticating username \"" + username + "\" using password");

		// authenticate principal in the realm
		final Principal principal = this.context.getRealm().authenticate(username, password);
		if (principal == null) {
			this.log.debug("failed to authenticate the user in the realm");
			return null;
		}

		// return the user descriptor
		return new AuthedUser(principal, username, password);
	}

	/**
	 * Process the authentication response and authenticate the user.
	 *
	 * @param session The session.
	 * @param request The request representing the authentication response.
	 *
	 * @return The authenticated user, or {@code null} if could not
	 * authenticate.
	 *
	 * @throws IOException If an I/O error happens communicating with the OP.
	 */
	protected AuthedUser processAuthResponse(final Session session, final Request request) throws IOException {
		this.log.warn("authenticating user using OpenID Connect authentication response");
		String username = null;
		Authorization authorization = null;
		GoogleIdToken.Payload payload = null;
		
		if(request.getParameter("credential")!=null && !request.getParameter("credential").equals("")){
			OPDescriptor googleProviderDesc = null;
			for(OPDescriptor opDescriptor : this.opDescs){
				if(opDescriptor.getName().equals("Google")){
					googleProviderDesc = opDescriptor;
				}
			}
			final String CLIENT_ID = googleProviderDesc.getClientId();
			
			NetHttpTransport transport = new NetHttpTransport();
			com.google.api.client.json.JsonFactory jsonFactory = new GsonFactory();
			GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
					.setAudience(Collections.singletonList(CLIENT_ID)).build();
			try {
				GoogleIdToken idToken = verifier.verify(request.getParameter("credential"));
				if (idToken != null) {
					payload = idToken.getPayload();
					username = (String) payload.getEmail();
					authorization = new Authorization(payload.getIssuer(), new Date(), request.getParameter("credential"),
							payload.getType(), payload.getExpirationTimeSeconds().intValue(), payload.getNonce(), "", payload.toPrettyString());
					
				}else{
					log.error("idtoken is null");
					return null;
				}
			}catch(GeneralSecurityException gse){
				log.error(gse.getMessage(), gse);
				return null;
			}
		}else {
			// parse the state
			final String stateParam = request.getParameter("state");
			if (stateParam == null) {
				this.log.warn("no state in the authentication response");
				return null;
			}
			final Matcher m = STATE_PATTERN.matcher(stateParam);
			if (!m.find()) {
				this.log.warn("invalid state value in the authentication response");
				return null;
			}
			final int opInd = Integer.parseInt(m.group(1));
			final String state = m.group(2);
			
			// get OP descriptor from the state
			if (opInd >= this.opDescs.size()) {
				this.log.warn("authentication response state contains invalid OP index");
				return null;
			}
			final OPDescriptor opDesc = this.opDescs.get(opInd);
			final String issuer = opDesc.getIssuer();
			this.log.warn("processing authentication response from " + issuer);
			
			// match the session id from the state
			final String sessionState = (String) session.getNote(SESS_STATE_NOTE);
			session.removeNote(SESS_STATE_NOTE);
			if (!state.equals(sessionState)) {
				this.log.warn("authentication response state does not match the session id");
				return null;
			}
			
			// check if error response
			final String errorCode = request.getParameter("error");
			if (errorCode != null) {
				final AuthErrorDesc authError = new AuthErrorDesc(request);
				this.log.warn("authentication error response: " + authError.getCode());
				request.setAttribute(AUTHERROR_ATT, authError);
				return null;
			}
			
			// get the authorization code
			final String authCode = request.getParameter("code");
			if (authCode == null) {
				this.log.warn("no authorization code in the authentication response");
				return null;
			}
			
			// call the token endpoint, check if error and get the ID token
			final TokenEndpointResponse tokenResponse = this.callTokenEndpoint(opDesc, authCode, request);
			final String tokenErrorCode = tokenResponse.responseBody.optString("error");
			if ((tokenResponse.responseCode != HttpURLConnection.HTTP_OK) || (tokenErrorCode.length() > 0)) {
				final AuthErrorDesc authError = new AuthErrorDesc(tokenResponse.responseBody);
				this.log.warn("token error response: " + authError.getCode());
				request.setAttribute(AUTHERROR_ATT, authError);
				return null;
			}
			
			// create the authorization object
			authorization = new Authorization(issuer, tokenResponse.responseDate, tokenResponse.responseBody);
			
			// decode the ID token
			final String[] idTokenParts = authorization.getIdToken().split("\\.");
			final JSONObject idTokenHeader = new JSONObject(new JSONTokener(new StringReader(new String(BASE64URL_DECODER.decode(idTokenParts[0]), UTF8))));
			final JSONObject idTokenPayload = new JSONObject(new JSONTokener(new StringReader(new String(BASE64URL_DECODER.decode(idTokenParts[1]), UTF8))));
			final byte[] idTokenSignature = BASE64URL_DECODER.decode(idTokenParts[2]);
			this.log.warn("decoded ID token:\n    header:    " + idTokenHeader + "\n    payload:   " + idTokenPayload + "\n    signature: " + Arrays.toString(idTokenSignature));
			
			// validate the ID token:
			
			// validate issuer match
			final String issValue = JsonUtilities.getOrDefault(idTokenPayload, "iss", null);
			if ((issValue == null) || !opDesc.getValidIssPattern().matcher(issValue).matches()) {
				this.log.warn("the ID token issuer is missing or does not match");
				return null;
			}
			
			// validate audience match
			final Object audValue = JsonUtilities.getOrDefault(idTokenPayload, "aud", "");
			boolean audMatch = false;
			if (audValue instanceof JSONArray) {
				final JSONArray auds = (JSONArray) audValue;
				for (int n = auds.length() - 1; n >= 0; n--) {
					if (opDesc.getClientId().equals(auds.get(n))) {
						audMatch = true;
						break;
					}
				}
			} else {
				audMatch = opDesc.getClientId().equals(audValue);
			}
			if (!audMatch) {
				this.log.warn("the ID token audience is missing or does not match");
				return null;
			}
			
			// validate authorized party
			if ((audValue instanceof JSONArray) && idTokenPayload.has("azp")) {
				if (!opDesc.getClientId().equals(idTokenPayload.get("azp"))) {
					this.log.warn("the ID token authorized party does not match");
					return null;
				}
			}
			
			// validate token expiration
			if (!idTokenPayload.has("exp") || (idTokenPayload.getLong("exp") * 1000L) <= System.currentTimeMillis()) {
				this.log.warn("the ID token expired or no expiration time");
				return null;
			}
			
			// validate signature
			if (!this.isSignatureValid(opDesc, idTokenHeader, idTokenParts[0] + '.' + idTokenParts[1], idTokenSignature)) {
				this.log.warn("invalid signature");
				return null;
			}
			this.log.warn("signature validated successfully");
			
			// the token is valid, proceed:
			
			// get username from the ID token
			JSONObject usernameClaimContainer = idTokenPayload;
			final String[] usernameClaimParts = opDesc.getUsernameClaimParts();
			for (int i = 0; i < usernameClaimParts.length - 1; i++) {
				final Object v = usernameClaimContainer.opt(usernameClaimParts[i]);
				if ((v == null) || !(v instanceof JSONObject)) {
					this.log.warn("the ID token does not contain the \"" + opDesc.getUsernameClaim() + "\" claim used as the username claim");
					return null;
				}
				usernameClaimContainer = (JSONObject) v;
			}
			username = usernameClaimContainer.optString(usernameClaimParts[usernameClaimParts.length - 1], null);
			if (username == null) {
				this.log.warn("the ID token does not contain the \"" + opDesc.getUsernameClaim() + "\" claim used as the username claim");
				return null;
			}
		}

		// authenticate the user in the realm
		this.log.warn("authenticating user \"" + username + "\"");
		
		this.context.getRealm();
		
		final Principal principal = this.context.getRealm().authenticate(username);
		if (principal == null) {
			this.log.warn("failed to authenticate the user in the realm");
			return null;
		}
		
		if(payload!=null) {
			session.getSession().setAttribute("userId", payload.getSubject());
			session.setNote("userId", payload.getSubject());
			session.getSession().setAttribute("name", payload.get("name"));
			session.setNote("name", payload.get("name"));
			session.setNote("emailVerified", Boolean.valueOf(payload.getEmailVerified()));
			session.getSession().setAttribute("pictureUrl", payload.get("picture"));
			session.setNote("pictureUrl", (String) payload.get("picture"));
			session.getSession().setAttribute("locale", payload.get("locale"));
			session.setNote("locale", (String) payload.get("locale"));
			session.getSession().setAttribute("displayName", payload.get("given_name") + " " + payload.get("family_name"));
			session.setNote("familyName", (String) payload.get("family_name"));
			session.setNote("givenName", (String) payload.get("given_name"));
		}
		
		// save authentication info in the session
		session.setNote(Constants.SESS_USERNAME_NOTE, principal.getName());
		session.setNote(SESS_OIDC_AUTH_NOTE, authorization);

		// save authorization in the session for the application
		session.getSession().setAttribute(AUTHORIZATION_ATT, authorization);

		// return the user descriptor
		return new AuthedUser(principal, username, "google-password");
	}

	/**
	 * Check if the JWT signature is valid.
	 *
	 * @param opDesc OP descriptor.
	 * @param header Decoded JWT header.
	 * @param data The JWT data (encoded header and payload).
	 * @param signature The signature from the JWT to test.
	 *
	 * @return {@code true} if valid.
	 *
	 * @throws IOException If an I/O error happens loading necessary data.
	 */
	protected boolean isSignatureValid(final OPDescriptor opDesc, final JSONObject header, final String data, final byte[] signature) throws IOException {
		try {
			final String sigAlg = header.optString("alg");
			switch (sigAlg) {
			case "RS256":
				final OPConfiguration opConfig = this.ops.getOPConfiguration(opDesc.getIssuer());
				if (opConfig == null) {
					this.log.warn("optional provider " + opDesc.getIssuer() + " could not be configured, reporting invalid token signature");
					return false;
				}
				final Signature sig = Signature.getInstance("SHA256withRSA");
				sig.initVerify(opConfig.getJWKSet().getKey(header.optString("kid", JWKSet.DEFAULT_KID)));
				sig.update(data.getBytes("ASCII"));

				return sig.verify(signature);

			case "HS256":
				if (opDesc.getClientSecret() == null) {
					this.log.warn("client secret required for HS256 signature algorithm is not configured, reporting signature invalid");
					return false;
				}

				final Mac mac = Mac.getInstance("HmacSHA256");
				mac.init(new SecretKeySpec(BASE64URL_DECODER.decode(opDesc.getClientSecret()), "HmacSHA256"));
				mac.update(data.getBytes("ASCII"));
				final byte[] genSig = mac.doFinal();

				return Arrays.equals(genSig, signature);

			default:
				this.log.warn("unsupported token signature algorithm \"" + sigAlg + "\", skipping signature verification");
				return true;
			}

		} catch (final NoSuchAlgorithmException | SignatureException | InvalidKeyException | UnsupportedEncodingException e) {
			throw new RuntimeException("Platform lacks signature algorithm support.", e);
		}
	}

	/**
	 * Call the OP's token endpoint and exchange the authorization code.
	 *
	 * @param opDesc OP descriptor.
	 * @param authCode The authorization code received from the authentication
	 * endpoint.
	 * @param request The request.
	 *
	 * @return The token endpoint response.
	 *
	 * @throws IOException If an I/O error happens communicating with the
	 * endpoint.
	 */
	protected TokenEndpointResponse callTokenEndpoint(final OPDescriptor opDesc, final String authCode, final Request request) throws IOException {
		// get the OP configuration
		final OPConfiguration opConfig = this.ops.getOPConfiguration(opDesc.getIssuer());
		if (opConfig == null) throw new RuntimeException("Optional provider " + opDesc.getIssuer() + " could not be configured.");
		final URL tokenEndpointURL = new URL(opConfig.getTokenEndpoint());

		// build POST body
		final StringBuilder buf = new StringBuilder(256);
		buf.append("grant_type=authorization_code");
		buf.append("&code=").append(URLEncoder.encode(authCode, UTF8.name()));
		buf.append("&redirect_uri=").append(URLEncoder.encode(this.getBaseURL(request) + Constants.FORM_ACTION, UTF8.name()));

		// configure connection
		final HttpURLConnection con = (HttpURLConnection) tokenEndpointURL.openConnection();
		con.setConnectTimeout(opDesc.getEndpointHttpConnectTimeout());
		con.setReadTimeout(opDesc.getEndpointHttpReadTimeout());
		con.setDoOutput(true);
		con.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.addRequestProperty("Accept", "application/json");
		con.setInstanceFollowRedirects(false);

		// configure authentication
		switch (opDesc.getTokenEndpointAuthMethod()) {
		case CLIENT_SECRET_BASIC:
			con.addRequestProperty("Authorization", "Basic " + BASE64_ENCODER.encodeToString( (opDesc.getClientId() + ":" + opDesc.getClientSecret()).getBytes(UTF8)));
			break;
		case CLIENT_SECRET_POST:
			buf.append("&client_id=").append(URLEncoder.encode( opDesc.getClientId(), UTF8.name()));
			buf.append("&client_secret=").append(URLEncoder.encode(opDesc.getClientSecret(), UTF8.name()));
			break;
		default:
			// nothing
		}

		// finish POST body and log the call
		final String postBody = buf.toString();
		this.log.debug("calling token endpoint at " + tokenEndpointURL + " with: " + postBody);

		// send POST and read response
		JSONObject responseBody;
		try (OutputStream out = con.getOutputStream()) {
			out.write(postBody.getBytes(UTF8.name()));
			out.flush();
			try (Reader in = new InputStreamReader(con.getInputStream(), UTF8)) {
				responseBody = new JSONObject(new JSONTokener(in));
			} catch (final IOException e) {
				final InputStream errorStream = con.getErrorStream();
				if (errorStream == null) throw e;
				try (Reader in = new InputStreamReader(errorStream, UTF8)) {
					responseBody = new JSONObject(new JSONTokener(in));
				}
			}
		}

		// create response object
		final TokenEndpointResponse response = new TokenEndpointResponse(con.getResponseCode(), con.getDate(), responseBody);

		// log the response
		this.log.debug("received response: " + response.toString());

		// return the response
		return response;
	}

	/**
	 * Process the case when the session expired while waiting for the user
	 * login input. If landing page is configured, tries to redirect to it.
	 * Otherwise, sends back an request timeout error response.
	 *
	 * @param request The request.
	 * @param response The response.
	 *
	 * @throws IOException If an I/O error happens communicating with the
	 * client.
	 */
	protected void processExpiredSession(final Request request, final HttpServletResponse response) throws IOException {
		// redirect to the configured landing page, if any
		if (!this.redirectToLandingPage(request, response))
			response.sendError(HttpServletResponse.SC_REQUEST_TIMEOUT, sm.getString("authenticator.sessionExpired"));
	}

	/**
	 * Redirect to the configured landing page, if any.
	 *
	 * @param request The request.
	 * @param response The response.
	 *
	 * @return {@code true} if successfully redirected, {@code false} if no
	 * landing page is configured.
	 *
	 * @throws IOException If an I/O error happens communicating with the
	 * client.
	 */
	protected boolean redirectToLandingPage(final Request request, final HttpServletResponse response) throws IOException {
		// do we have landing page configured?
		if (this.landingPage == null) return false;

		// construct landing page URI
		final String uri = request.getContextPath() + this.landingPage;

		// make it think the user originally requested the landing page
		final SavedRequest savedReq = new SavedRequest();
		savedReq.setMethod("GET");
		savedReq.setRequestURI(uri);
		savedReq.setDecodedRequestURI(uri);
		request.getSessionInternal(true).setNote(Constants.FORM_REQUEST_NOTE, savedReq);

		// send the redirect
		response.sendRedirect(response.encodeRedirectURL(uri));

		// done, success
		return true;
	}

	/**
	 * Get web-application base URL (either from the {@code hostBaseURI}
	 * authenticator property or auto-detected from the request).
	 *
	 * @param request The request.
	 *
	 * @return Base URL.
	 */
	protected String getBaseURL(final Request request) {
		if (this.hostBaseURI != null) return this.hostBaseURI + request.getContextPath();

		final StringBuilder baseURLBuf = new StringBuilder(64);
		baseURLBuf.append("https://").append(request.getServerName());
		final int port = request.getServerPort();
		if (port != 443) baseURLBuf.append(':').append(port);
		
		baseURLBuf.append(request.getContextPath());

		return baseURLBuf.toString();
	}

	
	////// BASIC GETTERS AND SETTERS
	
	// Get virtual host base URI property.
	public String getHostBaseURI() {
		return this.hostBaseURI;
	}
	
	// Set virtual host base URI property. The URI is used when constructing callback URLs for the web-application. If not set, the authenticator will attempt to construct it using the requests it receives.
	// @param hostBaseURI Host base URI. Must not end with a "/". Should be an HTTPS URI.
	public void setHostBaseURI(final String hostBaseURI) {
		
		this.hostBaseURI = hostBaseURI;
	}
	
	//Get providers configuration.
	public String getProviders() {
		return this.providers;
	}
	
	// @param providers The providers configuration, which is a JSON-like array of descriptors, one for each configured provider. Unlike standard JSON,
	// the syntax does not use double quotes around the property names and values (to make it XML attribute value friendly). The value can be
	// surrounded with single quotes if it contains commas, curly braces or whitespace.
	public void setProviders(final String providers) {
		this.providers = providers;
	}
	
	// Get name of the claim in the ID Token used as the username.
	public String getUsernameClaim() {
		return this.usernameClaim;
	}
	
	// Set name of the claim in the ID Token used as the username in the users realm. The default is "sub".
	public void setUsernameClaim(final String usernameClaim) {
		this.usernameClaim = usernameClaim;
	}
	
	// Get additional scopes for the authorization endpoint.
	public String getAdditionalScopes() {
		return this.additionalScopes;
	}
	
	// Set additional scopes for the authorization endpoint. The scopes are added to the required "openid" scope, which is always included.
	// @param additionalScopes The additional scopes as a space separated list.
	public void setAdditionalScopes(final String additionalScopes) {
		this.additionalScopes = additionalScopes;
	}
	
	// Tell if form-based authentication is disabled. @return {@code true} if disabled.
	public boolean isNoForm() {
		return this.noForm;
	}
	
	// Set flag that tells if the form-based authentication should be disabled.
	// @param noForm {@code true} to disabled form-based authentication.
	public void setNoForm(final boolean noForm) {
		this.noForm = noForm;
	}
	
	// Get HTTP connect timeout used for server-to-server communication with the
	// OpenID Connect provider. @return Timeout in milliseconds.
	public int getHttpConnectTimeout() {
		return this.httpConnectTimeout;
	}
	
	/**
	 * Set HTTP connect timeout used for server-to-server communication with the
	 * OpenID Connect provider. The default is 5000.
	 *
	 * @param httpConnectTimeout Timeout in milliseconds.
	 *
	 * @see java.net.URLConnection#setConnectTimeout(int)
	 */
	public void setHttpConnectTimeout(final int httpConnectTimeout) {
		this.httpConnectTimeout = httpConnectTimeout;
	}
	
	/**
	 * Get HTTP read timeout used for server-to-server communication with the OpenID Connect provider.
	 *
	 * @return Timeout in milliseconds.
	 */
	public int getHttpReadTimeout() {
		return this.httpReadTimeout;
	}
	
	/**
	 * Set HTTP read timeout used for server-to-server communication with the OpenID Connect provider. The default is 5000.
	 *
	 * @param httpReadTimeout Timeout in milliseconds.
	 *
	 * @see java.net.URLConnection#setReadTimeout(int)
	 */
	public void setHttpReadTimeout(final int httpReadTimeout) {
		this.httpReadTimeout = httpReadTimeout;
	}
	
	
}
