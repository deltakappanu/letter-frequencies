package org.bsworks.catalina.authenticator.oidc;

import java.security.Principal;

/**
 * Authenticated user descriptor.
 */
public final class AuthedUser {
	
	/**
	 * Principal.
	 */
	public final Principal principal;
	
	/**
	 * Username.
	 */
	public final String username;
	
	/**
	 * Password, if any.
	 */
	public final String password;
	
	
	/**
	 * Create new authenticated user descriptor.
	 *
	 * @param principal Principal.
	 * @param username  Username.
	 * @param password  Password, or {@code null} if not applicable.
	 */
	public AuthedUser(final Principal principal,
	                  final String username, final String password) {
		
		this.principal = principal;
		this.username = username;
		this.password = password;
	}
}
