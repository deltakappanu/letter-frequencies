package org.bsworks.util.json;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtilities {
	public static String getOrDefault(JSONObject o, String key, String defaultValue){
		try{
			return o.getString(key);
		}catch(JSONException je){
			return defaultValue;
		}
	}
}
